"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];




const trainerCardContainer = document.querySelector('.trainers-cards__container');
const trainerCardTemplate = document.getElementById("trainer-card").content;
console.log(trainerCardTemplate);

const trainerModalTemplate = document.getElementById("modal-template").content;
console.log(trainerModalTemplate);


let scrollPosition;
function scrollListener() {
	window.scrollTo(0, scrollPosition);
};


function createModalWindow(trainer) {
	const modalWindowFragment = document.importNode(trainerModalTemplate, true);
	console.log("This is modal window", modalWindowFragment);

	const modalPhoto = modalWindowFragment.querySelector('img');
	modalPhoto.setAttribute("src", trainer.photo);
	console.log("this is modal photo", modalPhoto);

	const modalName = modalWindowFragment.querySelector(".modal__name");
	modalName.textContent = `${trainer ["first name"]} ${trainer ["last name"]}`;
	console.log("this is modal name", modalName);

	const category = modalWindowFragment.querySelector(".modal__point--category");
	category.textContent = trainer.category;
	console.log("this is modal category", category);

	const experience = modalWindowFragment.querySelector(".modal__point--experience");
	experience.textContent = trainer.experience;
	console.log("this is modal experience", experience);

	const specialization = modalWindowFragment.querySelector(".modal__point--specialization");
	specialization.textContent = trainer.specialization;
	console.log("this is modal specialization", specialization);

	const description = modalWindowFragment.querySelector(".modal__text");
	description.textContent = trainer.description;
	console.log("this is modal description", description);

	const modalCloseBtn = modalWindowFragment.querySelector(".modal__close");
	modalCloseBtn.addEventListener('click', function(){
		document.querySelector(".modal").remove();
		document.removeEventListener('scroll', scrollListener);
	});

	const modalWindowContainer = modalWindowFragment.querySelector(".modal");
	trainerCardContainer.append(modalWindowContainer);


};

function resolveTrainerFilter(title) {switch (title) {
	case "Бійцівський клуб": return "fight-club";
	case "Дитячий клуб": return "kids-club";
	case "Басейн": return "swimming-pool";
	case "Тренажерний зал": return "gym";
	case "спеціаліст": return "specialist";
	case "майстер": return "master";
	case "інструктор" : return "instructor";
	default: throw "who is " + title;
} };

Array.from(DATA).forEach((trainer, index) => {

	const card = document.importNode(trainerCardTemplate, true);
	const trainerCard = card.querySelector(".trainer");
	trainerCard.id = index;
	console.log("This is our card", trainerCard);

	const photo = card.querySelector('img');
	photo.setAttribute("src", trainer.photo); 

	const name = card.querySelector(".trainer__name");
	name.textContent = `${trainer ["first name"]} ${trainer ["last name"]}`;

	const allTrainerDirection = "all-direction";
	const specificTrainerDirection = resolveTrainerFilter(trainer.specialization);
	trainerCard.classList.add(allTrainerDirection, specificTrainerDirection);
	const allTrainerCategory = "all-category";
	const specificTrainerCategory = resolveTrainerFilter(trainer.category);
	trainerCard.classList.add(allTrainerCategory, specificTrainerCategory);
	

	const trainerInfoBtn = card.querySelector(".trainer__show-more");
	trainerInfoBtn.addEventListener('click', function(){
		createModalWindow(trainer);
		scrollPosition = window.scrollY;
		
		document.addEventListener('scroll', scrollListener);

	});


	trainerCardContainer.append(card);
}); 














const sortingPanel = document.querySelector(".sorting");
sortingPanel.removeAttribute('hidden');

const sortingBtnList = sortingPanel.querySelectorAll(".sorting__btn");
console.log(sortingBtnList);
sortingBtnList.forEach(btn => {
	btn.addEventListener("click", function(event) {
		sortingBtnList.forEach(btn => {
			if (btn === event.target){
				btn.classList.add('sorting__btn--active');
			} else {
				btn.classList.remove('sorting__btn--active');
			};
		});
	});
});
sortingBtnList[0].addEventListener('click', function(){
	const trainerCards = document.querySelector(".trainers-cards__container");
	console.log("This is trainer cards children", trainerCards.children);
	[...trainerCards.children]
		.sort((a, b) => a.id - b.id)
		.forEach(node => trainerCards.appendChild(node));
});



function getLastName(trainerID) {
	return DATA[trainerID]["last name"];	
};

sortingBtnList[1].addEventListener('click', function() {
	const trainerCards = document.querySelector(".trainers-cards__container");
	[...trainerCards.children].sort((a,b) => 
	getLastName(a.id).localeCompare(getLastName(b.id), 'ua'))
	.forEach(node => trainerCards.appendChild(node));
});



function getExperience(trainerID) {
	return parseInt(DATA[trainerID].experience);	
};

sortingBtnList[2].addEventListener('click', function() {
	const trainerCards = document.querySelector(".trainers-cards__container");
	[...trainerCards.children].sort((a,b) => 
		getExperience(b.id) - getExperience(a.id))
	.forEach(node => trainerCards.appendChild(node));
});
	




const sidebar = document.querySelector(".sidebar");
sidebar.removeAttribute('hidden');
console.log("This is sidebar",sidebar);

function applyFilters (filters, trainerCard) {
	const isShown = filters.every(filter => {
		return	trainerCard.classList.contains(filter.id);
	});
	if(isShown) {
		trainerCard.removeAttribute('hidden');
	} else {
		trainerCard.setAttribute('hidden', true);
	}
};

const submitFilterForm = document.querySelector('form');
console.log("submit form", submitFilterForm);

submitFilterForm.addEventListener('submit', function(event) {
	event.preventDefault();
	const filters = document.querySelectorAll('input.filters__input:checked');
	const filtersArray = Array.from(filters)
	const trainerCards = document.querySelectorAll('li.trainer');

	trainerCards.forEach(card => applyFilters(filtersArray, card));

});








	// console.log("this is set direction", setTrainerDirection(card));

	// function setTrainerCategory(trainer) {
		
	// 	if (trainer.specialization.textContent === "майстер") {
	// 		trainerCard.setAttribute("value", "master");
	// 	} else if (trainer.specialization.textContent === "спеціаліст") {
	// 		trainerCard.setAttribute("value", "specialist");
	// 	} else if (trainer.specialization.textContent === "інструктор") {
	// 		trainerCard.setAttribute("value", "instructor");
	// 	} else { 
	// 		trainerCard.setAttribute("value", "all");
	// 	}
	// };
	// console.log("this is set category", setTrainerCategory(trainer));




	// function filterSpecialization (trainer) {
	// 	const specialisationFilter = trainer.specialization.textContent
	// 		if (sidebarFilters.item(1).checked) {
	// 			 === ''
	// 		}
		
	// }


